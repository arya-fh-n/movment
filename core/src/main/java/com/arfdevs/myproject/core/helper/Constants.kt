package com.arfdevs.myproject.core.helper

object Constants {

    const val SHARED_PREF_FILE = "movment_preferences"

    const val ONBOARDING_KEY = "onboarding_key"

    const val THEME_KEY = "theme_key"

    const val LANGUAGE_KEY = "language_key"

    const val USER_ID = "user_id"

    const val WISHLIST_TABLE = "wishlist"
    const val CART_TABLE = "cart"

    const val BACKDROP_PATH = "https://image.tmdb.org/t/p/w500"

    const val INITIAL_PAGE_INDEX = 1

}