package com.arfdevs.myproject.core.helper

import java.io.IOException

class NoConnectivityException : IOException("No internet connection exception")
